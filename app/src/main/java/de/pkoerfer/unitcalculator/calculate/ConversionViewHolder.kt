package de.pkoerfer.unitcalculator.calculate

import android.view.View
import android.widget.TextView

import de.pkoerfer.unitcalculator.R

/**
 *
 */
internal class ConversionViewHolder(rootView: View) {

    private val textView: TextView = rootView.findViewById(R.id.conversion_name)

    fun updateText(conversion: Conversion) {
        textView.setText(conversion.id)
    }
}
