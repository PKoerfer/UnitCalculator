package de.pkoerfer.unitcalculator.calculate

import de.pkoerfer.unitcalculator.core.MvpPresenter
import de.pkoerfer.unitcalculator.core.MvpView

/**
 *
 */
internal class CalculateContract {

    internal interface Presenter : MvpPresenter {
        fun calculate(value: Float, selectedPosition: Int)

        fun createConversions()
    }

    internal interface View : MvpView<Presenter> {
        fun updateUi(result: String)

        fun onConversionsCreated(conversions: Array<Conversion>)
    }
}
