package de.pkoerfer.unitcalculator.calculate

import android.util.Log

/**
 *
 */
internal class CalculatePresenter(private val view: CalculateContract.View) : CalculateContract.Presenter, ConversionSource.ConversionListener {
    private val calculator: Calculator = Calculator()
    private val conversionSource: ConversionSource = ConversionSource()

    override fun start() {
        conversionSource.addConversionListener(this)
    }

    override fun stop() {
        conversionSource.removeConversionListener(this)
    }

    override fun createConversions() {
        conversionSource.createConversions()
    }

    override fun calculate(value: Float, selectedPosition: Int) {
        val conversion = conversionSource.getConversion(selectedPosition)
        Log.d(TAG, "Found Conversion: " + conversion.name)
        val result: Float
        when (conversion) {
            Conversion.CELSIUS_TO_FAHRENHEIT, Conversion.FAHRENHEIT_TO_CELSIUS -> result = calculator.calculateTemperature(value, conversion)
            else -> result = calculator.calculateVolume(value, conversion)
        }
        view.updateUi(result.toString())
    }

    override fun onConversionsCreated(conversions: Array<Conversion>) {
        view.onConversionsCreated(conversions)
    }

    companion object {
        private val TAG = CalculatePresenter::class.java.simpleName
    }
}
