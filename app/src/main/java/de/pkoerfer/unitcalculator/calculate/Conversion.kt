package de.pkoerfer.unitcalculator.calculate

import android.support.annotation.StringRes

import de.pkoerfer.unitcalculator.R

/**
 *
 */
enum class Conversion constructor(@param:StringRes @field:StringRes @get:StringRes
                                          val id: Int) {
    METRIC_TO_IMPERIAL(R.string.conversion_metric_to_imperial),
    IMPERIAL_TO_METRIC(R.string.conversion_imperial_to_metric),
    CUPS_TO_OUNCES(R.string.conversion_cups_to_ounces),
    CUPS_TO_MILLILITER(R.string.conversion_cups_to_milliliter),
    OUNCES_TO_CUPS(R.string.conversion_ounces_to_cups),
    OUNCES_TO_MILLILITER(R.string.conversion_ounces_to_milliliter),
    MILLILITER_TO_CUPS(R.string.conversion_milliliter_to_cups),
    MILLILITER_TO_OUNCES(R.string.conversion_milliliter_to_ounces),
    CELSIUS_TO_FAHRENHEIT(R.string.conversion_celsius_to_fahrenheit),
    FAHRENHEIT_TO_CELSIUS(R.string.conversion_fahrenheit_to_celsius)
}
