package de.pkoerfer.unitcalculator.calculate

/**
 *
 */
internal class ConversionSource {

    private val conversionListener = mutableListOf<ConversionListener>()

    interface ConversionListener {
        fun onConversionsCreated(conversions: Array<Conversion>)
    }

    fun createConversions() {
        conversionListener.forEach { listener -> listener.onConversionsCreated(Conversion.values()) }
    }

    fun getConversion(selectedPosition: Int): Conversion {
        return Conversion.values()[selectedPosition]
    }

    fun addConversionListener(conversionListener: ConversionListener) {
        this.conversionListener.add(conversionListener)
    }

    fun removeConversionListener(conversionListener: ConversionListener) {
        this.conversionListener.remove(conversionListener)
    }
}
