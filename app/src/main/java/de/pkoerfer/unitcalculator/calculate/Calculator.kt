package de.pkoerfer.unitcalculator.calculate

/**
 *
 */
internal class Calculator {

    fun calculateTemperature(value: Float, conversion: Conversion): Float {
        if (Conversion.CELSIUS_TO_FAHRENHEIT === conversion) {
            return value * Constants.CELSIUS_TO_FAHRENHEIT_MULTIPLIER + Constants.CELSIUS_TO_FAHRENHEIT_OFFSET
        } else if (Conversion.FAHRENHEIT_TO_CELSIUS === conversion) {
            return (value - Constants.CELSIUS_TO_FAHRENHEIT_OFFSET) * Constants.FAHRENHEIT_TO_CELSIUS_MULTIPLIER
        }
        return value
    }

    fun calculateVolume(value: Float, conversion: Conversion): Float {
        return value * getRatio(conversion)
    }

    private fun getRatio(conversion: Conversion): Float {
        when (conversion) {
            Conversion.METRIC_TO_IMPERIAL -> return Constants.CENTIMETER_TO_INCHES_RATIO
            Conversion.IMPERIAL_TO_METRIC -> return Constants.INCHES_TO_CENTIMETER_RATIO
            Conversion.CUPS_TO_OUNCES -> return Constants.CUPS_TO_OUNCES_RATIO
            Conversion.CUPS_TO_MILLILITER -> return Constants.US_CUPS_TO_MILLILITER_RATIO
            Conversion.OUNCES_TO_CUPS -> return Constants.OUNCES_TO_CUPS_RATIO
            Conversion.OUNCES_TO_MILLILITER -> return Constants.OUNCES_TO_MILLILITER_RATIO
            Conversion.MILLILITER_TO_CUPS -> return Constants.MILLILITER_TO_US_CUPS_RATIO
            Conversion.MILLILITER_TO_OUNCES -> return Constants.MILLILITER_TO_OUNCES_RATIO
            else -> return 1f
        }
    }
}
