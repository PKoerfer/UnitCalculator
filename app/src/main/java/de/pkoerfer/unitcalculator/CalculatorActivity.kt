package de.pkoerfer.unitcalculator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import de.pkoerfer.unitcalculator.calculate.CalcFragment

/**
 *
 */
class CalculatorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ac_calculator)
        supportFragmentManager.beginTransaction().apply {
            add(R.id.ac_calculator_contentHolder, CalcFragment(), CalcFragment::class.java.simpleName)
            commit()
        }
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) { }
}
