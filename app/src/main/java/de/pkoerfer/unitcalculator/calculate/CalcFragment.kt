package de.pkoerfer.unitcalculator.calculate

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Spinner
import android.widget.TextView
import de.pkoerfer.unitcalculator.R
import java.util.*

/**
 *
 */
class CalcFragment : Fragment(), CalculateContract.View {

    private lateinit var imperialTextInputLayout: TextView
    private lateinit var conversionSpinner: Spinner
    private lateinit var presenter: CalculateContract.Presenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater!!.inflate(R.layout.fr_calculator_overview, container, false)
        if (null != root) {
            val metricTextInputLayout: TextInputLayout = root.findViewById(R.id.calculator_metric_input)
            imperialTextInputLayout = root.findViewById(R.id.calculator_result)
            conversionSpinner = root.findViewById(R.id.calculator_conversion_spinner)
            metricTextInputLayout.editText!!.inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_CLASS_NUMBER
            metricTextInputLayout.editText!!.setImeActionLabel("", 6)
            metricTextInputLayout.editText!!.setOnEditorActionListener { v, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    presenter.calculate(java.lang.Float.valueOf(v.text.toString())!!, conversionSpinner.selectedItemPosition)
                }
                false
            }
        }
        return root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.createConversions()
    }

    override fun updateUi(result: String) {
        imperialTextInputLayout.text = result
    }

    override fun onConversionsCreated(conversions: Array<Conversion>) {
        val dataAdapter = ConversionAdapter(context, Arrays.asList(*conversions))
        conversionSpinner.adapter = dataAdapter
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter = CalculatePresenter(this)
        presenter.start()
    }

    override fun onDetach() {
        presenter.stop()
        super.onDetach()
    }
}
