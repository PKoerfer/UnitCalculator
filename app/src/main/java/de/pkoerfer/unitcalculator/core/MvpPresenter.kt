package de.pkoerfer.unitcalculator.core

/**
 *
 */
interface MvpPresenter {
    fun start()
    fun stop()
}
