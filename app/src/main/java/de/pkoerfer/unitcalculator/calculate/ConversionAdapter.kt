package de.pkoerfer.unitcalculator.calculate

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter

import de.pkoerfer.unitcalculator.R

/**
 *
 */
class ConversionAdapter internal constructor(context: Context, private val conversions: List<Conversion>?) : BaseAdapter() {

    private val inflater: LayoutInflater = context.applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var root = convertView
        val viewHolder: ConversionViewHolder
        if (null == root) {
            root = inflater.inflate(R.layout.cell_conversion, null)
            viewHolder = ConversionViewHolder(root!!)
            root.tag = viewHolder
        } else {
            viewHolder = root.tag as ConversionViewHolder
        }

        val conversion = getItem(position)
        if (null != conversion) {
            viewHolder.updateText(conversion)
        }
        return root
    }

    override fun getCount(): Int {
        return conversions?.size ?: 0
    }

    override fun getItem(position: Int): Conversion? = conversions?.let {
        if (it.size > position) return it[position] else null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }
}
